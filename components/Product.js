import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Item, Input, Thumbnail, Text, Body, Button, Icon, Right, Left, Card, CardItem, Title } from 'native-base';
import { StyleSheet, Image, FlatList, ListView, ActivityIndicator, ActivityIndicatorIOS, Dimensions } from 'react-native';
import Category from './Category';
import { setTimeout } from 'core-js/library/web/timers';
import { server } from './server';
import { AsyncStorage } from 'react-native';
import Pagination from "react-js-pagination";

const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Cochin',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  priceText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  priceTextPromo: {
    textDecorationLine: 'line-through',
  },
  promoText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  listItem: {
    width: '100%',
    marginLeft: 0,
    paddingLeft: 10,
    paddingRight: 0,
    marginRight: 0
  }
});
export default class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      page: 1,
      currentPage: 1,
      prev: false,
      next: false,
      pagination: [
        {prev: false},
        {next: false}
      ],
      data: [],
      promoData: [],
      currentCategory:0,
      categories: [],
      sliderWidth: Dimensions.get('window').width,
      itemWidth: Dimensions.get('window').width * 80 / 100
    };
  };
  fetchList(){
    var categories_q = this.state.currentCategory!=0?'&categories='+this.state.currentCategory:'';
    fetch(server + '/user/items?page='+this.state.currentPage+categories_q, {
      mode: "no-cors",
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }).then(response => {
        var pagination = [
          {prev: false},
          {next: false}
        ];
      if (response.headers.has('content-range'))
        {
          var total = parseInt(response.headers.get('content-range').split('/').pop());
          var last = parseInt(response.headers.get('content-range').split('-').pop());
          var first = parseInt(response.headers.get('content-range').split(' ').pop());
          var maxpage = Math.floor(total/(last-first))+1;
          if (this.state.currentPage < maxpage)
          {
            pagination[1].next = true;
          }
          if (this.state.currentPage > 1)
          {
            pagination[0].prev = true;
          }
          this.setState({pagination:pagination});
        }
      return response.json()}).then(response => {
      this.setState({ isLoading: false });
      this.setState({ data: response });
    }).catch((error) => {
      console.log(error);
    });
  }
  componentDidMount() {
    // Items list
    this.fetchList();
    // PROMO
    var categories_q = this.state.currentCategory!=0?'&categories='+this.state.currentCategory:'';
    fetch(server + '/user/items?page='+this.state.currentPage+categories_q, {
      mode: "no-cors",
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }).then(response => {
      return response.json()}).then(response => {
      this.setState({ isLoading: false });
      this.setState({ promoData: response.filter(item => { if (item.price != item.promo_price) return item; }) });
      }).catch((error) => {
      console.log(error);
    });
    // Categories list
    fetch(server + '/user/categories/', {
      mode: "no-cors",
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }).then(response => response.json()).then(response => {
      this.setState({ isLoading: false });
      this.setState({ categories: response });
      }).catch((error) => {
      console.log(error);
    });
  }
  static navigationOptions = {
    tabBarLabel: 'Home',
    // Note: By default the icon is only shown on iOS. Search the showIcon option below.
    tabBarIcon: ({ tintColor }) => (
      <Icon name="apps" />
    ),
  };
  render() {

    if (this.state.isLoading == true) {
      return (
        <Container>
          <Header searchBar>
            <Item>
              <Icon name="ios-search" />
              <Input placeholder="Search" />
              <Icon name="cart" />
            </Item>
            <Button transparent>
              <Text>Search</Text>
            </Button>
          </Header>
          <Content>
            <ActivityIndicator />
          </Content>
        </Container>
      );
    }
    return (
      <Container>
        <Header>
          <Body>
            <Title>
              PRODUCTS
            </Title>
          </Body>
        </Header>
        <Content>
          {/* LIST OF CATEGORIES */}
          <List>
            <ListItem itemDivider>
                <Body>
                  <Title>CATEGORIES</Title>
                </Body>
                <Right>
                  <Button rounded light onPress={()=>{
                    this.setState({currentCategory: 0});
                    this.fetchList();
                  }}>
                  <Title>{"<<"} ALL</Title>
                  </Button>
                </Right>
              </ListItem>        
          </List>
          <FlatList
            horizontal
            data={this.state.categories}
            renderItem={({ item: rowData }, index) => {
              return (
                <Card>
                  <CardItem button onPress={
                    ()=>{
                      this.setState({currentCategory: rowData.id});
                      return this.fetchList();
                    }
                  }>
                    <Title>
                      {rowData.name.toUpperCase()}
                    </Title>
                  </CardItem>
                </Card>
              );
            }}
            keyExtractor={(item, index) => index}
          />
          {/* LIST PROMO ITEMS */}
          <List>
            <ListItem itemDivider>
                <Left/>
                <Body>
                  <Title>SALE OFF</Title>
                </Body>
                <Right/>
              </ListItem>        
          </List>
          <FlatList
            horizontal
            data={this.state.promoData}
            renderItem={({ item: rowData }) => {
              return (
                <Card >
                  <CardItem>
                    <Left />
                    <Title>
                      {rowData.name}
                    </Title>
                    <Right />
                  </CardItem >
                  <CardItem cardBody button onPress={() => this.props.navigation.navigate('ProductDetail', { id: rowData.id })}>
                    <Left />
                    <Body>
                      <Thumbnail source={rowData.images == null ? require('../images/product.png') : { uri: rowData.images }} />
                    </Body>
                    <Right />
                  </CardItem>
                  <CardItem>
                    <Left />
                    <Body>
                      <Title>{rowData.promo_price} VND</Title>
                    </Body>
                    <Right />
                  </CardItem>
                </Card>
              );
            }}
            keyExtractor={(item, index) => index}
          />
          
          {/* PRODUCTS LIST */}
          <List>
            <ListItem itemDivider>
                <Left/>
                <Body>
                  <Title>PRODUCTS</Title>
                </Body>
                <Right/>
              </ListItem>        
          </List>
          <List dataArray={this.state.data} renderRow={(item) => (
            <ListItem avatar onPress={() => this.props.navigation.navigate('ProductDetail', { id: item.id })} style={styles.listItem}>
              <Left>
                <Thumbnail source={item.images == null ? require('../images/product.png') : { uri: item.images }} />
              </Left>
              <Body>
                <Text>{item.name}</Text>
                {item.price != item.promo_price ? (
                  <Body>
                    <Text note style={styles.priceTextPromo}>{item.price} VND</Text>
                    <Text style={styles.promoText}>{item.promo_price} VND</Text>
                  </Body>)
                  :
                  (<Body>
                    <Text style={styles.priceText}>{item.price} VND</Text>
                  </Body>)}
              </Body>
            </ListItem>
          )}>
          </List>
          <Card>
            <CardItem>
              <Body>
              <FlatList
            horizontal
            data={this.state.pagination}
            renderItem={({ item: rowData }, index) => { 
              if (rowData.prev == true|| rowData.next ==true)
              return (
                <Card>
                  <CardItem button onPress={()=>{
                    if (Object.getOwnPropertyNames(rowData)=='prev')
                      this.setState({currentPage: this.state.currentPage-1});
                    if (Object.getOwnPropertyNames(rowData)=='next')
                      this.setState({currentPage: this.state.currentPage+1});  
                    return this.fetchList();}}>
                  <Title>
                    {Object.getOwnPropertyNames(rowData)}
                    </Title>  
                  </CardItem>
                </Card>
              );
              }}
              keyExtractor={(item, index) => index}/>
              </Body>
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}