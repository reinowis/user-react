import React, { Component } from 'react';
import { Container, Header, Content, Title, List, ListItem, Thumbnail, Text, Body, Left, Right, Button, Image, Icon, Label, Input, Form, Item, Toast } from 'native-base';
import { server } from './server';
import {AsyncStorage} from 'react-native';
export default class Login extends Component {
    static navigationOptions = {
      tabBarLabel: 'Login',
      // Note: By default the icon is only shown on iOS. Search the showIcon option below.
      tabBarIcon: ({ tintColor }) => (
        <Icon name="apps" />
      ),
      headerTitle: 'Login'
    };
    constructor(props){
      super(props);
      this.state ={
        username:'',
        password:'',
        checkUsername:false,
        checkPassword:false,
        checkLogin: false,
      }
    };
    changeUsername = value => {
      this.setState({username: value});
      
    };
    changePassword = value =>{
      this.setState({password: value});
      
    };
    onSubmitForm(){
      if (this.state.username=='')
        {
          this.setState({checkUsername: true});
          return;
        }
      if (this.state.password=='')
        {
          this.setState({checkPassword: true});
          return;
        }
        fetch(server+'/rest-auth/login/', {
          mode: "no-cors",  
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body:JSON.stringify({username: this.state.username, password: this.state.password})
        }).then(response => {
          if (response.status < 200 || response.status >= 400)
            throw Toast.show({
              text: "Please check the entered values",
              position: "top",
              type: "danger",
              buttonText: "OK"
            });
          return response.json()})
        .then(({key})=>{
            AsyncStorage.setItem('security',JSON.stringify({token:key, username: this.state.username}))
          }).then(()=>
            {this.props.navigation.navigate('Home');}
        )
        .catch(error => console.log(error));
    };
    onRegister(){
      this.props.navigation.navigate('Signup');
    };
    render() {
      return (
        <Container>
        <Header>
        <Body>
            <Title>Login</Title>
          </Body>
        </Header>
        <Content>
          <Form>
            <Item inlineLabel error={this.state.checkUsername}>
              <Label>Username</Label>
              <Input onChangeText={this.changeUsername}/>
              <Icon name="close-circle" active={this.state.checkUsername}/>
            </Item>
            <Item inlineLabel last error={this.state.checkPassword}>
              <Label>Password</Label>
              <Input secureTextEntry={true} onChangeText={this.changePassword}/>
              <Icon name="close-circle" active={this.state.checkPassword}/>
            </Item>
            <Item>
              <Left/>
              <Body>
              <Button rounded onPress={this.onSubmitForm.bind(this)} ><Text>Submit</Text></Button>
              </Body>
              <Right/>
            </Item>
            <Item/>
            <Item>
              <Body>
                <Text>Haven't had an account yet?</Text>
              </Body>
            </Item>
            <Item>
              <Left></Left>
              <Body>
              <Button onPress={this.onRegister.bind(this)} rounded warning><Text>Register</Text></Button>
              </Body>
              <Right></Right>
            </Item>
          </Form>
        </Content>
      </Container>
      );
    }
  }