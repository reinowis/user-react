import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Item, Input, Thumbnail, Text, Body, Button, Icon, Right, Left, Card, CardItem } from 'native-base';
import { StyleSheet, Image, FlatList, ListView, ActivityIndicator, ActivityIndicatorIOS } from 'react-native';
import Category from './Category';
import { setTimeout } from 'core-js/library/web/timers';
import { server } from './server';
import { AsyncStorage } from 'react-native';
import RelatedDetail from './RelatedDetail';
const styles = StyleSheet.create({
  baseText: {
    fontFamily: 'Cochin',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  priceText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  priceTextPromo: {
    textDecorationLine: 'line-through',
  },
  promoText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  listItem: {
    width: '100%',
    marginLeft: 0,
    paddingLeft: 10,
    paddingRight: 0,
    marginRight: 0
  }
});
export default class Related extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      related: []
    };
  };
  componentDidMount(){ 
    console.log(this.props.related);
    this.setState({related: this.props.related});
  }
  static navigationOptions = {
    tabBarLabel: 'Home',
    // Note: By default the icon is only shown on iOS. Search the showIcon option below.
    tabBarIcon: ({ tintColor }) => (
      <Icon name="apps" />
    ),
  };
  render() {
    
    if (this.props.related.length == 0)
    {
      return (
        <Text>There isn't any related item</Text>
      );
    }
    return (
      
      <List
        style={{flexDirection: 'column'}}
        dataArray={this.props.related}
        renderRow={ item => {
          return (
            // <div style={{ padding: 0, width: 160 }}>
            // <Thumbnail source={{ uri: 'https://lh3.googleusercontent.com/gN6iBKP1b2GTXZZoCxhyXiYIAh8QJ_8xzlhEK6csyDadA4GdkEdIEy9Bc8s5jozt1g=w300' }} />
             <RelatedDetail {...this.props} id={item}/>
           );
        }}
      />
    );
  }
}