import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Left, Right, List, ListItem, Thumbnail, Text, Body, Button, Icon, Toast, Title } from 'native-base';
import {Image, FlatList, ActivityIndicator} from 'react-native';
import { server } from './server';
import { styles } from './styles';
export default class RelatedDetail extends Component {
    static navigationOptions = {
      tabBarLabel: 'Home',
      // Note: By default the icon is only shown on iOS. Search the showIcon option below.
      tabBarIcon: ({ tintColor }) => (
        <Icon name="apps" />
      ),
      headerTitle: 'Product Detail'
    };
    constructor(props){
      super(props);
      this.state = {
       isLoading: true,
       item: {
         id: this.props.id,
         name: '',
         price: 0,
         quantity: 0,
         info: '',
         images: null,
         categories: [],
         related: []
       },
       quantity: 1, 
      };
    };
    
    componentDidMount(){
      fetch(server+'/user/items/'+this.props.id, {
        mode: "no-cors",  
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        }
      }).then(respondse =>{
        if (respondse.status < 200 || respondse.status >= 300)
          throw Toast.show({
            text: respondse.statusText?respondse.statusText:'Error connection',
            position: 'bottom',
            buttonText: 'Okay'
          });
        return respondse.json();
      }).then(respondse=> {
        this.setState({isLoading: false});
        this.setState({item:{
         id: respondse.id,
         name: respondse.name,
         promo_price: respondse.promo_price,
         price: respondse.price,
         quantity: respondse.quantity,
         info: respondse.info,
         images: respondse.images,
         categories: respondse.categories,
         related: respondse.related
        }});
      }).catch(error => console.log(error));
    };
    render(){
      if (this.state.isLoading == true){
        return (
           <Card>
             <CardItem>
              <ActivityIndicator/>
             </CardItem>
           </Card>
        );
      }
      return (
        <ListItem avatar onPress={() => this.props.navigation.navigate('ProductDetail', { id: this.state.item.id })} style={styles.listItem}>
            <Left>
              <Thumbnail source={this.state.item.images==null?require('../images/product.png'):{uri: this.state.item.images }} />
            </Left>
            <Body>
              <Text>{this.state.item.name}</Text>
              {this.state.item.price!=this.state.item.promo_price?(
                <Body>
              <Text note style={styles.priceTextPromo}>{this.state.item.price} VND</Text>  
              <Text style={styles.promoText}>{this.state.item.promo_price} VND</Text>
              </Body>)
              :
              (<Body>
                <Text style={styles.priceText}>{this.state.item.price} VND</Text>  
                </Body>)}
            </Body>
          </ListItem>
      );
    }  
    }