import React, { Component } from 'react';
import { AsyncStorage, ActivityIndicator } from 'react-native';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Body, Button, Image, Icon, Card, CardItem, Title, Left, Right, Toast, Form, Item, Label, Input } from 'native-base';
import Login from './Login';
import {server} from './server';
export default class Account extends Component {
  static navigationOptions = {
    tabBarLabel: 'Account',
    // Note: By default the icon is only shown on iOS. Search the showIcon option below.
    tabBarIcon: ({ tintColor }) => (
      <Icon name="person" />
    ),
  };

  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      username: '',
      oldpassword: '',
      password: '',
      password2: '',
      checkOldPassword: false,
      checkPassword: false,
      checkPassword2: false,
      token: ''
    };
  }
  changeUsername = value => {
    this.setState({ username: value });

  };
  changeOldPassword = value => {
    this.setState({ oldpassword: value });
  };
  changePassword = value => {
    this.setState({ password: value });
  };
  changePassword2 = value => {
    if (value != this.state.password)
      return this.setState({ checkPassword2: true })
    this.setState({ password2: value });
  };
  onSubmitForm() {
    if (this.state.oldpassword == '') {
      this.setState({ checkOldPassword: true });
      return;
    }
    if (this.state.password == '') {
      this.setState({ checkPassword: true });
      return;
    }
    if (this.state.password2 == ''||this.state.password2!=this.state.password) {
      this.setState({ checkPassword2: true });
      return;
    }
    fetch(server + '/rest-auth/password/change/', {
      mode: "no-cors",
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token '+this.state.token,
      },
      body: JSON.stringify({ new_password1: this.state.password, new_password2: this.state.password2, oldpassword: this.state.oldpassword })
    }).then(respondse => {
      if (respondse.status < 200 || respondse.status >=300)
        throw Toast.show({
          text: respondse.statusText?respondse.statusText:'Error connection',
          position: 'bottom',
          buttonText: 'Okay'
      });
      Toast.show({
        text: 'Changed password successfully',
        position: 'bottom'
      });
      return this.props.navigation.navigate("Home");
    })
      .catch(error => console.log(error));
  };
  onLogout(){
    AsyncStorage.removeItem('security').then( result =>{
      this.setState({username:'',token:''});
      this.props.navigation.navigate("Home");
    }
    );
  }
  // if !(localStorage.getItem('username')&&localStorage.getItem('token')) 
  //   this.props.navigation.navigate('Login');
  componentDidMount() {
    AsyncStorage.getItem('security', (err, result) => {
      if (!err && result != null) {
        // do something
        result = JSON.parse(result);
        this.setState({ username: result.username });
        this.setState({token: result.token});
        this.setState({ isLoading: false });
      }
      else {
        console.log(err);
      }
    });
  }
  render() {
    // if (this.state.isLoading == true)
    //   return (
    //     <Container>
    //       <Header>
    //         <Body>
    //           <Title>Account</Title>
    //         </Body>
    //       </Header>
    //       <Content>
    //         <ActivityIndicator />
    //       </Content>
    //     </Container>
    //   );
    if (this.state.username != '')
      return (
        <Container>
          <Content>
            <Header>
              <Left>
                <Icon name='person' />
              </Left>
              <Body>
                <Title>{this.state.username}</Title>
              </Body>
              <Right />
            </Header>
            <Card>
              <CardItem>
                <Left />
                <Body>
                  <Icon name='person' />
                  <Title>{this.state.username}</Title>
                </Body>
                <Right />
              </CardItem>
            </Card>
            <Card>
                <CardItem>
                  <Body>
                    <Title>Change password</Title>
                  </Body>
                </CardItem>
                <Form>
                <Item floatingLabel last error={this.state.checkOldPassword}>
                    <Label>Old password</Label>
                    <Input secureTextEntry={true} onChangeText={this.changeOldPassword} />
                    <Icon name="close-circle" active={this.state.checkOldPassword} />
                  </Item>
                  <Item floatingLabel last error={this.state.checkPassword}>
                    <Label>New password</Label>
                    <Input secureTextEntry={true} onChangeText={this.changePassword} />
                    <Icon name="close-circle" active={this.state.checkPassword} />
                  </Item>
                  <Item floatingLabel last error={this.state.checkPassword2}>
                    <Label>Re-type new password</Label>
                    <Input secureTextEntry={true} onChangeText={this.changePassword2} />
                    <Icon name="close-circle" active={this.state.checkPassword2} />
                  </Item>
                  </Form>
                  <CardItem>
                    <Left />
                    <Body>
                      <Button rounded onPress={this.onSubmitForm.bind(this)} ><Text>Submit</Text></Button>
                    </Body>
                    <Right />
                </CardItem>
            </Card>
            <Card>
              <CardItem>
                <Left/>
                <Body>
                  <Button rounded danger onPress={this.onLogout.bind(this)}>
                    <Text>Log Out</Text>
                  </Button>
                </Body>
                <Right/>
              </CardItem>
            </Card>
          </Content>
        </Container>
      );
    else {
      return (
        <Login {...this.props} />
      );
    }

  }
}