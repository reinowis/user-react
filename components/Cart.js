import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Image, Badge, List, ListItem, Thumbnail, ActionSheet, Card, CardItem, Item, Label, Form, Input, Toast } from 'native-base';
// import { server } from './service/connect';
// import ProductList from './components/ProducList';
import { styles } from './styles';
import { server } from './server';
import { ActivityIndicator } from 'react-native';
export default class Cart extends Component {
  static navigationOptions = {
    tabBarLabel: 'Cart',
    tabBarIcon: ({ tintColor }) => (
      <Icon name="cart" />
    ),
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      products: [],
      products_id: [],
      total: 0,
      address:'',
      phone:'',
      checkAddress: false,
      checkPhone: false,
      token: '',
    };
  }
  changeAddress(value){
    this.setState({address: value});
  }
  changePhone(value){
    this.setState({phone: value});
  }
  onSubmit(){
    if (this.state.token == "")
      {
        return Toast.show({
          text: "Please sign in to order",
          position: "bottom",
          type: "danger"
        });
      }
    if (this.state.products.length == 0 || this.state.products_id.length == 0)
      {
        return Toast.show({
          text: "Empty cart",
          position: "bottom",
          type: "warning"
        })
      }    
    if (this.state.address=='')
        {
          this.setState({checkAddress: true});
          return;
        }
      if (this.state.phone=='')
        {
          this.setState({checkPhone: true});
          return;
        }
      var products = [];
      products.map(item => {
        var temp = {
          item_id: item.id,
          quantity: item.quantity,
          orders_id: null
        };
        products.push(temp);
      });  
        fetch(server+'/user/orders/', {
          mode: "no-cors",  
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Token '+this.state.token
          },
          body:JSON.stringify({
            phone: this.state.phone, 
            address: this.state.address,
            details: products
          })
        }).then(response => {
          if (response.status < 200 || response.status >= 300)
            throw new Toast({
              text: response.json(),
              position: "bottom",
              type: "danger"
            });
          return response.json()})
        .then(result=>{
          AsyncStorage.multiRemove(this.state.products_id).then(()=>{
              this.setState({isLoading: true});
              this.setState({total: 0});
              this.setState({products: []});
              this.setState({products_id: []});  
              this.loadList();
          }).then(()=>
            {this.props.navigation.navigate('Home');}
        ).catch(error => console.log(error));
  });
}
  loadList(){
    var temp = this.state.products_id;
    AsyncStorage.getAllKeys().then(result => {
      result.map(i => {
        if (i.startsWith("products_"))
          temp.push(i);
      });
      this.setState({ products_id: temp });
    }).then(() => {
      AsyncStorage.multiGet(this.state.products_id, (err, result) => {
        if (!err && (result != null && result != '')) {
          // result = JSON.parse(result);
          this.setState({ products: result });
        }
        else
          console.log(err);
      }).then(()=>{
        var temp =[];
        this.state.products.map((value)=>{
          temp.push([value[0], JSON.parse(value[1])]);
        });
        return temp;
      }).then(result => this.setState({products: result})).then(()=>{
        this.state.products.map((value)=>{
          this.setState({ isLoading: false });
          this.setState({total: this.state.total+=value[1].total})
        })
      });
    });
  }
  componentDidMount() {
    this.loadList();
    AsyncStorage.getItem("security").then(result=>{
      if (result != null)
        this.setState({token: JSON.parse(result).token})  
    }
    );
  }
  render() {
    if (this.state.isLoading == true) {
      return (
        <Container>
          <Header>
            <Body>
              <Title>Cart</Title>
            </Body>
          </Header>
          <Content>
            <ActivityIndicator />
            <Title>Empty cart</Title>
            <Card>
          <Form>
            <Item floatingLabel>
              <Label>Address:</Label>
              <Input onChangeText={this.changeAddress.bind(this)}/>
              <Icon name="close-circle" active={this.state.checkAddress}/>
            </Item>
            <Item floatingLabel>
              <Label>Phone number:</Label>
              <Input onChangeText={this.changePhone.bind(this)}/>
              <Icon name="close-circle" active={this.state.checkPhone}/>
            </Item>
            <Item>
              <Left/>
              <Body>
                <Button rounded success onPress={this.onSubmit.bind(this)}><Text>SUBMIT</Text></Button>
              </Body>
              <Right/>
            </Item>
          </Form>
        </Card>
          </Content>
        </Container>
      );
    }
    return (<Container>
      <Header>
        <Body>
          <Title>Cart</Title>
        </Body>
      </Header>
      <Content>
        <List dataArray={this.state.products} renderRow={(item) => (
          <ListItem avatar style={styles.listItem} onPress={()=>{
            ActionSheet.show(
              {
                options: ["DELETE", "Cancel"],
                cancelButtonIndex: 1,
                destructiveButtonIndex: 0,
                title: "ACTION"
              },
              buttonIndex => {
                if (buttonIndex==0){
                  AsyncStorage.removeItem(item[0]).then(()=>{
                    this.setState({isLoading: true});
                    this.setState({total: 0});
                    this.setState({products: []});
                    this.setState({products_id: []});  
                    this.loadList();
                  });
                }
              }
            );
            
          }}
          >
            <Left>
              <Thumbnail source={item[1].images==null?require('../images/product.png'):{uri: item[1].images}} />
            </Left>
            <Body>
              <Text>{item[0]}</Text>
              <Text note>{item[1].quantity}</Text>
            </Body>
            <Right>
              <Text style={styles.promoText}>{item[1].total}</Text>
            </Right>
          </ListItem>
        )}>
        </List>
        <List>
          <ListItem itemDivider>
          </ListItem>
        </List>
        <Card>
          <CardItem>
            <Left><Title>PRICE</Title></Left>
            <Body><Title>{this.state.total} VND</Title></Body>
          </CardItem>
        </Card>
        <List>
          <ListItem itemDivider>
            <Body>
              <Title>CUSTOMER ADRESS</Title>
            </Body>
          </ListItem>
        </List>
        <Card>
          <Form>
            <Item floatingLabel>
              <Label>Address:</Label>
              <Input onChangeText={this.changeAddress.bind(this)}/>
              <Icon name="close-circle" active={this.state.checkAddress}/>
            </Item>
            <Item floatingLabel>
              <Label>Phone number:</Label>
              <Input onChangeText={this.changePhone.bind(this)}/>
              <Icon name="close-circle" active={this.state.checkPhone}/>
            </Item>
            <Item>
              <Left/>
              <Body>
                <Button rounded success onPress={this.onSubmit.bind(this)}><Text>SUBMIT</Text></Button>
              </Body>
              <Right/>
            </Item>
          </Form>
        </Card>
      </Content>
    </Container>
    );
  }
}
