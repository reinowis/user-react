import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text, Image, style } from 'native-base';
// import { server } from './service/connect';
// import ProductList from './components/ProducList';
export default class CartDetail extends Component {
  static navigationOptions = {
    tabBarLabel: 'Notifications',
    tabBarIcon: ({ tintColor }) => (
      <Icon name="apps" />
    ),
  };

  render() {
    return (
      <Container>
        <Content>
        <List onPress={() => this.props.navigation.navigate('ProductDetail', { user: 'Lucy' })}>
            <ListItem avatar onPress={() => this.props.navigation.navigate('ProductDetail', { user: 'Lucy' })} style={styles.listItem}>
              <Left>
                <Thumbnail source={{ uri: 'https://lh3.googleusercontent.com/gN6iBKP1b2GTXZZoCxhyXiYIAh8QJ_8xzlhEK6csyDadA4GdkEdIEy9Bc8s5jozt1g=w300' }} />
              </Left>
              <Body>
                <Text>Title</Text>
                <Text note style={styles.priceTextPromo}>20,000</Text>
                <Text style={styles.promoText}>10,000</Text>
              </Body>
            </ListItem>
            <ListItem avatar onPress={() => this.props.navigation.navigate('ProductDetail', { user: 'Lucy' })} style={styles.listItem}>
              <Left>
                <Thumbnail source={{ uri: 'https://lh3.googleusercontent.com/gN6iBKP1b2GTXZZoCxhyXiYIAh8QJ_8xzlhEK6csyDadA4GdkEdIEy9Bc8s5jozt1g=w300' }} />
              </Left>
              <Body>
                <Text>Title</Text>
                <Text note style={styles.priceTextPromo}>20,000</Text>
                <Text style={styles.promoText}>10,000</Text>
              </Body>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}
