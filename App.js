import React, { Component } from 'react';
import { Container, Header, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon, Text } from 'native-base';
// import { server } from './service/connect';
import ProductDetail from './components/ProductDetail';
import Product from './components/Product';
import Cart from './components/Cart';
import CartDetail from './components/CartDetail';
import OrderDetail from './components/OrderDetail';
import History from './components/History';
import Account from './components/Account';
import Login from './components/Login';
import Signup from './components/Signup';
import { Root } from "native-base";
import { TabNavigator, StackNavigator } from "react-navigation";
import { AsyncStorage } from 'react-native';
const AppNavigator = TabNavigator({
  Product: {
    screen: Product,
  },
  Cart: {
    screen: Cart,
  },
  History: {
    screen: History,
  },
  Account: {
    screen: Account,
  },
}, {
  tabBarPosition: 'bottom',
  animationEnabled: true,
  tabBarOptions: {
    activeTintColor: '#e91e63',
  },
});
const MainNavigator = StackNavigator({
  Home: { 
    screen: AppNavigator,
    navigationOptions: ({navigation}) => ({
    header: null
    }),
  },
  ProductDetail: { screen: ProductDetail },
  OrderDetail: { screen: OrderDetail },
  Login: { screen: Login },
  Signup: { screen: Signup },
});
// AsyncStorage.removeItem("products");
// AsyncStorage.setItem("products","")
// AsyncStorage.clear();
export default () =>
  <Root>
    <MainNavigator />
  </Root>;