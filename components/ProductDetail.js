import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Left, Right, List, ListItem, Thumbnail, Text, Body, Button, Icon, Toast, Title } from 'native-base';
import { Image, FlatList, ActivityIndicator, AsyncStorage } from 'react-native';
import { server } from './server';
import { styles } from './styles';
import Related from './Related';
export default class ProductDetail extends Component {
  static navigationOptions = {
    headerTitle: 'Product Detail'
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      item: {
        id: this.props.navigation.state.params.id,
        name: '',
        price: 0,
        promo_price: 0,
        quantity: 0,
        info: '',
        images: null,
        categories: [],
        related: []
      },
      quantity: 1,
      products: [],
      info : {
        id: '',
        name: '',
        quantity: 0,
        price: 0,
        images: null,
        total: 0
      },
    };
  };
  addToCart() {
        info = {
          id:this.state.item.id,
          name: this.state.item.name,
          quantity: this.state.item.quantity,
          images: this.state.item.images,
          price: this.state.item.promo_price, 
          total: this.state.item.promo_price*this.state.item.quantity,
        };
        AsyncStorage.setItem("products_"+this.state.item.id, JSON.stringify(info)).then(()=>{
          this.props.navigation.navigate("Cart");
        });
  };
  componentDidMount() {
    fetch(server + '/user/items/' + this.props.navigation.state.params.id, {
      mode: "no-cors",
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    }).then(respondse => {
      if (respondse.status < 200 || respondse.status >= 300)
        throw Toast.show({
          text: respondse.statusText ? respondse.statusText : 'Error connection',
          position: 'bottom',
          buttonText: 'Okay'
        });
      return respondse.json();
    }).then(respondse => {
      this.setState({ isLoading: false });
      this.setState({
        item: {
          id: respondse.id,
          name: respondse.name,
          price: respondse.price,
          quantity: respondse.quantity,
          info: respondse.info,
          images: respondse.images,
          categories: respondse.categories,
          related: respondse.related,
          promo_price: respondse.promo_price
        }
      });
    }).catch(error => console.log(error));
  };
  render() {
    if (this.state.isLoading == true) {
      return (
        <Container>
          <Content>
            <ActivityIndicator />
          </Content>
        </Container>
      );
    }
    return (
      <Container>
        <Content>
          <Card>
            <CardItem cardBody>
              <Left />
              <Body>
                <Thumbnail source={this.state.item.images != null ? { uri: this.state.item.images } : require('../images/product.png')} />
              </Body>
              <Right />
            </CardItem>
            <CardItem>
              <Left />
              <Body>
                <Title>{this.state.item.name}</Title>
              </Body>
              <Right />
            </CardItem>
          </Card>

          {(this.state.item.quantity > 0)
            ? (
              <Card>
                <CardItem>
                  <Left>
                    <Button rounded bordered danger onPress={() => {
                      if (this.state.quantity > 1)
                        this.setState({ quantity: this.state.quantity -= 1 });
                    }}>
                      <Text>-</Text>
                    </Button>
                  </Left>
                  <Body>
                    <Title>{this.state.quantity}</Title>
                  </Body>
                  <Right>
                    <Button rounded bordered sucsess onPress={() => {
                      if (this.state.quantity < this.state.item.quantity)
                        this.setState({ quantity: this.state.quantity += 1 });
                    }}>
                      <Text>+</Text>
                    </Button>
                  </Right>
                </CardItem>
                <CardItem>
                  <Body>
                    <Button rounded success onPress={this.addToCart.bind(this)}>
                      <Text>ADD TO CART</Text>
                    </Button>
                  </Body>
                  <Right />
                </CardItem>
              </Card>
            ) : ''
          }
          <Card>
            <CardItem>
              <Left>
                <Title>Price:</Title>
              </Left>
            {this.state.item.price != this.state.item.promo_price ? (
                  <Body>
                    <Text note style={styles.priceTextPromo}>{this.state.item.price} VND</Text>
                    <Text style={styles.promoText}>{this.state.item.promo_price} VND</Text>
                  </Body>)
                  :
                  (<Body>
                    <Text style={styles.priceText}>{this.state.item.price} VND</Text>
                  </Body>)}
            </CardItem>
            <CardItem>
              <Left>
                <Title>Quantity:</Title>
              </Left>
              <Body>
                <Text>{this.state.item.quantity > 0 ? this.state.item.quantity : 'SOLD OUT'}</Text>
              </Body>
              <Right />
            </CardItem>
            <CardItem>
              <Left>
                <Title>Info:</Title>
              </Left>
              <Body>
                <Text>{this.state.item.info}</Text>
              </Body>
              <Right />
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Related {...this.props} related={this.state.item.related} />
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}