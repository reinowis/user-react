import React, { Component } from 'react';
import { Container, Header, Content, Card, CardItem, Left, Right, List, ListItem, Thumbnail, Text, Body, Button, Icon, Toast, Title } from 'native-base';
import { Image, FlatList, ActivityIndicator, AsyncStorage } from 'react-native';
import { server } from './server';
import { styles } from './styles';
import Related from './Related';
export default class OrderDetail extends Component {
  static navigationOptions = {
    headerTitle: 'Order Detail'
  };
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
      order: {
        id: this.props.navigation.state.params.order.id,
        status: this.props.navigation.state.params.order.status,
        price: this.props.navigation.state.params.order.price,
        date: this.props.navigation.state.params.order.date,
        address: this.props.navigation.state.params.order.address,
        phone: this.props.navigation.state.params.order.phone,
      },
      token: this.props.navigation.state.params.token,
      details: []
    };
  };
  componentDidMount() {
    fetch(server + '/user/orderdetail/?orders=' + this.props.navigation.state.params.order.id, {
      mode: "no-cors",
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + this.props.navigation.state.params.token
      }
    }).then(respondse => {
      if (respondse.status < 200 || respondse.status >= 300)
        throw Toast.show({
          text: respondse.statusText ? respondse.statusText : 'Error connection',
          position: 'bottom',
          buttonText: 'Okay'
        });
      return respondse.json();
    }).then(respondse => {
      this.setState({ isLoading: false });
      this.setState({ details: respondse });
    }).catch(error => console.log(error));
  };
  render() {
    if (this.state.isLoading == true) {
      return (
        <Container>
          <Content>
            <ActivityIndicator />
          </Content>
        </Container>
      );
    }
    return (
      <Container>
        <Content>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail source={require('../images/product.png')} />
              </Left>
              <Body><Title>ORDER #{this.state.order.id}</Title></Body>
            </CardItem>
            <CardItem>
              <Left><Title>Date:</Title></Left>
              <Body><Text node>{Date(this.state.order.date).toString()}</Text></Body>
            </CardItem>
            <CardItem>
              <Left><Title>Total:</Title></Left>
              <Body><Text style={styles.promoText}>{this.state.order.price} VND</Text></Body>
            </CardItem>
            <CardItem>
              <Left><Title>Address:</Title></Left>
              <Body><Text>{this.state.order.address}</Text></Body>
            </CardItem>
            <CardItem>
              <Left><Title>Phone:</Title></Left>
              <Body><Text>{this.state.order.phone}</Text></Body>
            </CardItem>
            <CardItem>
              <Left><Title>Status:</Title></Left>
              <Body>{(() => {
                switch (this.state.order.status.toString()) {
                  case "-1": return (<Button danger><Text>CANCELLED</Text></Button>);
                    break;
                  case "0": return (<Button primary><Text>PROCESSING</Text></Button>);
                    break;
                  case "1": return (<Button warning><Text>DELIVERED</Text></Button>);
                    break;
                  case "2": return (<Button success><Text>RECEIVED</Text></Button>);
                    break;
                }
              })()}</Body>
            </CardItem>
          </Card>
          <Card>
            <List dataArray={this.state.details} renderRow={(item) => (
              <ListItem avatar style={styles.listItem}>
                <Left>
                  <Thumbnail source={require('../images/product.png')} />
                </Left>
                <Body>
                  <Text>ID #{item.item}</Text>
                  <Text note>{item.quantity} x {item.price} VND</Text>
                </Body>
                <Right>
                  <Text style={styles.promoText}>{item.total} VND</Text>
                </Right>
              </ListItem>
            )} />
          </Card>
        </Content>
      </Container>
    );
  }
}