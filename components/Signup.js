import React, { Component } from 'react';
import { Container, Header, Content, Title, List, ListItem, Thumbnail, Text, Body, Left, Right, Button, Image, Icon, Label, Input, Form, Item, Toast } from 'native-base';
import { server } from './server';
import {AsyncStorage} from 'react-native';
export default class Signup extends Component {
    static navigationOptions = {
      headerTitle: 'Sign Up'
    };
    constructor(props){
      super(props);
      this.state ={
        username:'',
        password:'',
        password2:'',
        checkUsername:false,
        checkPassword:false,
        checkPassword2:false,
      }
    };
    changeUsername = value => {
      this.setState({username: value});
    };
    changePassword = value =>{
      this.setState({password: value});
    };
    changePassword2 = value =>{
      this.setState({password2: value});
    };
    onSubmitForm(){
      if (this.state.username==''||this.state.username.length<3)
        {
          this.setState({checkUsername: true});
          Toast.show({
            text:"Username is not allowed to be empty and must be more than 3 characters",
            position: "top",
            type: "warning",
            buttonText: "OK"
          });
          return;
        }

      if (this.state.password==''||this.state.password.length<8)
        {
          this.setState({checkPassword: true});
          Toast.show({
            text:"Password is not allowed to be empty and must be more than 8 characters",
            position: "top",
            type: "warning",
            buttonText: "OK"
          });
          return;
        }
      if (this.state.password!= this.state.password2){
        this.setState({checkPassword2: true});
        Toast.show({
          text:"Password is not matched",
          position: "top",
          type: "warning",
          buttonText: "OK"
        });
        return;
      }  
        fetch(server+'/rest-auth/registration/', {
          mode: "no-cors",  
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body:JSON.stringify({username: this.state.username, password1: this.state.password, password2: this.state.password2})
        }).then(response => {
          if (response.status < 200 || response.status >= 300)
            throw Toast.show({
              text: "Please check the entered value or choose another username",
              position: "top",
              type: "danger",
              buttonText: "OK"
            });
          return response.json()})
        .then(({key})=>{
            AsyncStorage.setItem('security',JSON.stringify({token:key, username: this.state.username}))
          }).then(()=>
            {this.props.navigation.navigate('Home');}
        )
        .catch(error => {
          Toast.show({
            text: "Please check the entered value or choose another username",
            position: "top",
            type: "danger",
            buttonText: "OK"
          });
          console.log(error)});
    };
    render() {
      return (
        <Container>
        <Content>
          <Form>
            <Item floatingLabel error={this.state.checkUsername}>
              <Label>Username</Label>
              <Input onChangeText={this.changeUsername}/>
              <Icon name="close-circle" active={this.state.checkUsername}/>
            </Item>
            <Item floatingLabel last error={this.state.checkPassword}>
              <Label>Password</Label>
              <Input secureTextEntry={true} onChangeText={this.changePassword}/>
              <Icon name="close-circle" active={this.state.checkPassword}/>
            </Item>
            <Item floatingLabel last error={this.state.checkPassword2}>
              <Label>Re-type Password</Label>
              <Input secureTextEntry={true} onChangeText={this.changePassword2}/>
              <Icon name="close-circle" active={this.state.checkPassword2}/>
            </Item>
            <Item>
              <Left/>
              <Body>
              <Button rounded onPress={this.onSubmitForm.bind(this)} ><Text>Submit</Text></Button>
              </Body>
              <Right/>
            </Item>
          </Form>
          <Text note>Minimum password length is 8 characters.</Text>
        </Content>
      </Container>
      );
    }
  }