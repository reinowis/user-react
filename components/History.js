import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Thumbnail, Text, Body, Button, Image, Icon, Left, Title, Right } from 'native-base';
import { styles } from './styles';
import { server } from './server';
import { ActivityIndicator, AsyncStorage } from 'react-native';
export default class History extends Component {
    static navigationOptions = {
      tabBarLabel: 'History',
      // Note: By default the icon is only shown on iOS. Search the showIcon option below.
      tabBarIcon: ({ tintColor }) => (
        <Icon name="pulse" />
      ),
    };
    constructor(props){
      super(props);
      this.state ={
        orders: [],
        isLoading: true,
        token: ''
      };

    }
    componentDidMount(){
      AsyncStorage.getItem("security").then(result=>{
        if (result != null)
          {
            this.setState({token: JSON.parse(result).token});  
          }
      }
      ).then(()=>{
        fetch(server + '/rest-auth/user/', {
          mode: "no-cors",
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Token '+this.state.token
          }
        }).then(response => response.json()).then(({id})=>{
          fetch(server + '/user/orders/?customer='+id, {
            mode: "no-cors",
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              'Authorization': 'Token '+this.state.token
            }
          }).then(response=>{
            return response.text();})
            .then(result=>{
            this.setState({orders: JSON.parse(result)});
            this.setState({isLoading: false});
          });
        });
        
      });
      
    };
    render() {
      const status =[
        "CANCELLED",
        "PROCESSING",
        "DELIVERED",
        "RECEIVED",
      ];
      if (this.state.isLoading == true || this.state.token == ""){
        return (
          <Container>
            <Content>
              <Header>
                <Left/>
                <Body><Title>HISTORY</Title></Body>
                <Right/>
              </Header>
              <ActivityIndicator/>
              {(this.state.token == "")?(<Text>Please sign in to view the history</Text>):(<Text>Empty </Text>)}
            </Content>
          </Container>  
        );
      }
      return ( <Container>
          <Content>
          <Header>
                <Left/>
                <Body><Title>HISTORY</Title></Body>
                <Right/>
              </Header>
              {(this.state.orders.length==0)?(<Title>Empty history</Title>):(<Text></Text>)}
          <List dataArray={this.state.orders} renderRow={item=>{
            return (
              <ListItem avatar onPress={() => this.props.navigation.navigate('OrderDetail',{order: item, token: this.state.token})} style={styles.listItem}>
              <Left>
                <Thumbnail source={require('../images/product.png')} />
              </Left>
              <Body>
                <Text>ORDER #{item.id}</Text>
                <Text style={styles.promoText}>{item.price} VND</Text>
                <Text node>{Date(item.date).toString()}</Text>
                  {(()=>{
                    switch(item.status.toString()){
                      case "-1": return (<Button danger><Text>CANCELLED</Text></Button>);
                      break;
                      case "0": return (<Button primary><Text>PROCESSING</Text></Button>);
                      break;
                      case "1": return (<Button warning><Text>DELIVERED</Text></Button>);
                      break;
                      case "2": return (<Button success><Text>RECEIVED</Text></Button>);
                      break;
                    }})()}
                    </Body>
            </ListItem>
            )
          }}/>
          </Content>
        </Container>
      );
    }
  }