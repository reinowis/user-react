import { StyleSheet, Image, FlatList } from 'react-native';
export const colors = {
  black: '#1a1917',
  gray: '#888888',
  background1: '#B721FF',
  background2: '#21D4FD'
};
export const styles = StyleSheet.create({
    baseText: {
      fontFamily: 'Cochin',
    },
    titleText: {
      fontSize: 20,
      fontWeight: 'bold',
    },
    priceText: {
      fontSize: 20,
      fontWeight: 'bold',
    },
    priceTextPromo: {
      textDecorationLine: 'line-through',
    },
    promoText: {
      fontSize: 20,
      fontWeight: 'bold',
    },
    listItem: {
      width: '100%',
      marginLeft: 0,
      paddingLeft: 10,
      paddingRight: 0,
      marginRight: 0
    },
    container: {
      flex: 1,
      backgroundColor: colors.background1
    },
    // gradient: {
    //     ...StyleSheet.absoluteFillObject
    // },
    scrollview: {
        flex: 1,
        paddingTop: 50
    },
    scrollviewContentContainer: {
        paddingBottom: 50
    },
    exampleContainer: {
        marginBottom: 30
    },
    title: {
        paddingHorizontal: 30,
        backgroundColor: 'transparent',
        color: 'rgba(255, 255, 255, 0.9)',
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    subtitle: {
        marginTop: 5,
        paddingHorizontal: 30,
        backgroundColor: 'transparent',
        color: 'rgba(255, 255, 255, 0.75)',
        fontSize: 13,
        fontStyle: 'italic',
        textAlign: 'center'
    },
    slider: {
        marginTop: 25
    },
    sliderContentContainer: {
    },
    paginationContainer: {
        paddingVertical: 8
    },
    paginationDot: {
        width: 8,
        height: 8,
        borderRadius: 4,
        marginHorizontal: 8
    }
  });